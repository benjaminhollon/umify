# Umify
Adds "like", "um", "y'know", and similar words randomly to websites.

Once available only as a Chrome extension, Umify has received new vitality, redesigned as a Greasemonkey script!

# Installation

Umify is now redesigned as a Greasemonkey script, meaning it works in just any web browser that supports them in some form!

## Firefox, Firefox-based, Chrome, and Chromium-based

This will cover the vast majority of people. You'll need to install a Greasemonkey-compatible extension and add the script to it. This guide will cover using the Tampermonkey extension, though the official Greasemonkey extension should work too.

### Installing the extension: Firefox and Firefox-based

For Firefox browsers, [install the extension from the Firefox Addons site](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/).

### Installing the extension: Chrome and Chromium-based

For Chromium-based browsers, [install the extension from the Chrome Web Store](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).

### Adding the script

Copy the script from [`umify.js`](https://codeberg.org/benjaminhollon/umify/src/branch/main/umify.js) in this repository. Click on the Tampermonkey extension icon and select "Create a new script..." Paste the full text of `umify.js` into the editor, replacing any text already present. Save the script.

Any pages you load from now on (that the extension is allowed to modify) will now be umified!

# Modification

By default, Umify applies to every site you visit. To change this, you can edit the `@match` key in the script to target specific sites. (Tampermonkey also has settings for this if you don't want to modify the script.)

# License

The Greasemonkey script version of Umify is public domain under 0BSD. (Previously, Umify was licensed under the GPL.)

---

Original Release Announcement: [Umify Available on the Chrome Web Store (outdated)](https://seewitheyesclosed.com/blog/article/umify-available/)
