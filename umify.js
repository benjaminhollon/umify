// ==UserScript==
// @name         Umify
// @namespace    https://umify.party
// @version      2.0.0
// @description  Makes the web read the way people talk
// @author       Benjamin Hollon
// @match        *://*/*
// ==/UserScript==

(function() {
	'use strict';

	//Declare variables
	var additions = ["like", "y'know", "um", "like, y'know", "like, um", "uh"];
	var replace = ["to", "of", "is", "do", "as", "All rights"];

	function textNodesUnder(root){ //Get all text nodes under element
		var textNodes = [];
		addTextNodes(root);
		[].forEach.call(root.querySelectorAll('*'),addTextNodes);
		return textNodes;

		function addTextNodes(el){
			textNodes = textNodes.concat(
				[].filter.call(el.childNodes,function(k){
					return k.nodeType==Node.TEXT_NODE;
				})
			);
		}
	}

	var textNodes = textNodesUnder(document.body); //Get all text nodes on page
	for (var node in textNodes) { //For every text node
		for (var word in replace) { //Up to one replacement per replacement type per text node
			if (textNodes[node].textContent.trim() != "") { //If the text node has content
				textNodes[node].nodeValue = textNodes[node].textContent.replace(" " + replace[word] + " ", " " + replace[word] + ", " + additions[Math.floor(Math.random() * additions.length)] + ", "); //Add a random word from additions after the word
			}
		}
	}
})();
